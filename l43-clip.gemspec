# frozen_string_literal: true

require_relative 'lib/l43_clip'
version = L43Clip::VERSION

description = <<~DESCRIPTION

  A command line oriented Clipboard Manager

  Binaries and libs which make Ruby awesome (for me)
DESCRIPTION

Gem::Specification.new do |s|
  s.name         = 'l43-clip'
  s.version      = version
  s.summary      = 'CLI Clipboard Manager'
  s.description  = description
  s.authors      = ["Robert Dober"]
  s.email        = 'robert.dober@gmail.com'
  s.files        = Dir.glob("lib/**/*.rb")
  s.files       += Dir.glob("bin/**/*")
  s.files       += %w[LICENSE README.md]
  s.bindir       = 'bin'
  s.executables += Dir.glob('bin/*').map { File.basename(_1) }
  s.homepage     = 'https://gitlab.com/l43-gems/l43-clip.git'
  s.licenses     = %w[Apache-2.0]

  s.required_ruby_version = '>= 3.2.0'

  s.add_dependency 'clipboard', '~> 1.3.6'
  s.add_dependency 'ex_aequo', '~> 0.2.6'
end
# SPDX-License-Identifier: Apache-2.0
