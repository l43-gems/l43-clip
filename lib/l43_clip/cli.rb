# frozen_string_literal: true

require 'clipboard'
require_relative 'runner'

module L43Clip
  module Cli
    extens self
    attr_reader :cursor

    def run
      init

      loop do
        print "cmd> "
        interpret
      end
    end

    private

    def help
      puts "Who needs help!"
    end

    def init
      @cursor = 0
      Runner.start
    end

    def interpret
      cmd = gets.chomp
      case cmd
      when "help", "h"
        help
      when "l", "list"
        list
      when "save", "s", "store"
        store
      when "q", "quit"
        puts "Problem with your <Ctrl> key?"
        break
      else
        puts "Illegal cmd #{cmd}"
        help
      end
    end

    def list
      Runner
        .history[cursor..cursor + 9]
        .each_with_index do |entry, idx|
          puts("%<nb>3d> %<entry>s" % {nb: idx + cursor, entry:})
        end
    end
  end
end

at_exit { L43Clip::Runner.store }
# SPDX-License-Identifier: Apache-2.0
