# frozen_string_literal: true

module L43Clip
  module Runner
    extend self
    attr_reader :thread

    def history
      thread.thread_variable_get('history')
    end

    def start
      @thread = ::Thread.start do
        # TODO: Read from file
        history = []
        Thread.current.thread_variable_set("history", history)
        loop do
          x = Clipboard.paste
          if x != history.first
            history = [x, *history].uniq
            Thread.current.thread_variable_set("history", history)
          end
          sleep 1
        end
      end
    end

    def store
      puts "storing state of #{self}"
    end
  end
end
# SPDX-License-Identifier: Apache-2.0
