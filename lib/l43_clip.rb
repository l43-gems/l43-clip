# frozen_string_literal: true

require 'clipboard'
module L43Clip
  extend self
  VERSION = '0.1.0'
end
# SPDX-License-Identifier: Apache-2.0
