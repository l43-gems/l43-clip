# frozen_string_literal: true

require_relative 'enumerable'
require_relative 'keyword_scanner'

module Tools
  module Color
    extend self
    module Colorizer
      extend self
      COLOR_START_RGX = %r{-(\w*?)::}

      def colorize_list(args, joiner:, indent:)
        (' ' * indent) +
          args.scan_map do |iterator, str, color|
            if Symbol === color
              iterator.advance(Color.color(str, color), 2)
            else
              iterator.advance(str, 1)
            end
          end.join(joiner)
      end

      def colorize_string(text)
        Tools::KeywordScanner
          .scan(text, COLOR_START_RGX)
          .map do |col, val|
            if col
              Color.color(val, col)
            else
              val
            end
          end
          .join
      end
    end

    def bold(str)
      "\e[30;0;1m#{str}\e[0m"
    end

    def color(string, col)
      return string.to_s if ENV['NO_COLOR']
      return string.to_s if col.to_s == 'none'
      return bold(string.to_s) if col.to_s == 'bold'

      # This is to ensure it works in the production environnement even w/o setting NO_COLOR
      # if we want colors in the production environnement we can quickly implement them as
      # in the `bold` method below
      string.to_s.send(col) rescue string.to_s
    end

    def color_chunk
      lambda do |text_color|
        color(text_color.first, text_color.last)
      end
    end

    def color_chunks(*args, sep: ' ', indent: 0)
      (' ' * indent) +
        args
        .each_slice(2)
        .map(&color_chunk)
        .join(sep)
    end

    def colorize(*args, indent: 0, joiner: ' ')
      if args.size == 1
        if String === args.first
          Colorizer.colorize_string(args.first)
        else
          Colorizer.colorize_list(args.first, indent:, joiner:)
        end
      else
        Colorizer.colorize_list(args, indent:, joiner:)
      end
    end
  end
end
